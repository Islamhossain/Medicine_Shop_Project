<?php 
	session_start();
	if(isset($_SESSION['name'])){
?>
<!DOCTYPE html>
<html lang="EN">
	<head>
		<meta charset="UTF-8">
		<title>Medicine_Shop_byIslam</title>
		<link href="../css/index_admin_style.css" rel="stylesheet" type="text/css">
		<link href="../css/home_admin_style.css" rel="stylesheet" type="text/css">
	</head>
	<body>	
	<div class="header">	
		<header>
			<?php  
				include("../html/header.php");	//header include section//		
			?>
		</header>
	</div>

	<div class="menu">
		<nav id="primary_nav_wrap">
			<?php   
				if($_SESSION['type']=='admin'){
					include('../html/menu.php');
				}else if($_SESSION['type']=='manager'){
					include('../html/menu_manager.php');
				}else if($_SESSION['type']=='shop_kepper'){
					include('../html/menu_shop_keper.php');
				}else{
					echo "Your User Permission is Blocked.";
				}//menu end	 //
			?>
		</nav>
	</div>

<br/>
<br/>
	<div class="container">
		<div class="container-fluid">		
			<?php		
				if(isset($_SESSION['id'])){
					
					include('user_home.php');
					
				}else{
					echo "Login Faild..";
					echo "<script>window.location='../index.php';</script>";
				}
			?>
		</div>
	</div>
<br/>
	<div class="footer">
		<div class="footer-fluid">
			<footer>
				<?php 
					include("../html/footer.php"); //footer include section// 
				?>
			</footer>
		</div>
	</div>
	
	</body>
</html>
<?php
	}else{
		echo "<script>alert('Thappor Khabi!');</script>";
		echo "<script>window.location='../index.php';</script>";
	}
?>