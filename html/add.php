<?php 
	session_start();
	if(isset($_SESSION['name'])){
?>
<!DOCTYPE html>
<html lang="EN">
	<head>
		<meta charset="UTF-8">
		<title>Medicine_Shop_byIslam</title>
		<link href="../css/index_admin_style.css" rel="stylesheet" type="text/css">
		<link href="../css/home_admin_style.css" rel="stylesheet" type="text/css">
		<link href="../css/add_menu_style.css" rel="stylesheet" type="text/css">
	</head>
	<body>	
	<div class="header">	
		<header>
			<?php  
				include("../html/header.php");	//header include section//		
			?>
		</header>
	</div>

	<div class="menu">
		<nav id="primary_nav_wrap">
			<?php   
				if($_SESSION['type']=='admin'){
					include('../html/menu.php');
				}else if($_SESSION['type']=='manager'){
					include('../html/menu_manager.php');
				}else if($_SESSION['type']=='shop_kepper'){
					include('../html/menu_shop_keper.php');
				}else{
					echo "Your User Permission is Blocked.";
				}//menu end	 //
			?>
		</nav>
	</div>

<br/>
<br/>
	<div class="container"><!--container body start -->
		<div class="container-fluid">		
			<?php		
				if(isset($_SESSION['id'])){
					if($_SESSION['type']=='admin'){
						include('../html/add_content_admin.php');
					}else if($_SESSION['type']=='manager'){
						include('../html/add_content_manager.php');
					}else if($_SESSION['type']=='shop_kepper'){
						include('../html/add_content_shop_kepper.php');
					}else{
						echo "<script>alert('I Think You Are Blocked!');</script>";
						echo "<script>window.location='../logout.php';</script>";
					}			
				}else{
					echo "Login Faild..";
					echo "<script>window.location='../index.php';</script>";
				}
			?>
		</div><!--container body end -->
	</div>
<br/>
	<div class="footer">
		<div class="footer-fluid">
			<footer>
				<?php 
					include("../html/footer.php"); //footer include section// 
				?>
			</footer>
		</div>
	</div>
	
	</body>
</html>
<?php
	}else{
		echo "<script>alert('Thappor Khabi!');</script>";
		echo "<script>window.location='../logout.php';</script>";
	}
 ?>